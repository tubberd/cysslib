#include <cyss/graphics/2D/Font.hpp>

#include <OGLFT.h>


using namespace cyss;



MonochromeFont::MonochromeFont(std::string _filename, float _size)
{
    OGLFT::Monochrome* font = new OGLFT::Monochrome(_filename.c_str(), _size);
    m_Handle = (void*)font;
}

MonochromeFont::~MonochromeFont()
{
    OGLFT::Monochrome* font = (OGLFT::Monochrome*)m_Handle;
    delete font;
}

void MonochromeFont::setSize(float _size)
{
    OGLFT::Monochrome* font = (OGLFT::Monochrome*)m_Handle;
    font->setPointSize(_size);
}

void MonochromeFont::setForegroundColor(unsigned char _r, unsigned char _g, unsigned char _b)
{
    OGLFT::Monochrome* font = (OGLFT::Monochrome*)m_Handle;
    float r,g,b;
    
    if(_r == 0) r = 0.0f;
    else r = ((float)255/_r) * 1.0f;
    if(_g == 0) g = 0.0f;
    else g = ((float)255/_g) * 1.0f;
    if(_b == 0) b = 0.0f;
    else b = ((float)255/_b) * 1.0f;
    
    font->setForegroundColor(r, g, b);
}

void MonochromeFont::draw(float _x, float _y, std::string _text)
{
    OGLFT::Monochrome* font = (OGLFT::Monochrome*)m_Handle;
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
    font->draw(_x, _y, _text.c_str());
}
