#include <cyss/sys/Entity.hpp>
#include <cyss/sys/IDGenerator.hpp>

using namespace cyss;
using namespace cyss::ecs;

Entity::Entity()
{
    m_UniqueID = IDGenerator::getInstancePtr()->getNextID();
}
Entity::Entity(std::string _name)
{
    m_UniqueID = IDGenerator::getInstancePtr()->getID(_name);
}
Entity::~Entity()
{
    for(auto componentPair : m_Components)
    {
        componentPair.second.reset();
    }
}

std::string Entity::getName()
{
    return m_Name;
}
size_t Entity::getID()
{
    return m_UniqueID;
}

void Entity::addComponent(Component* _component)
{
    m_Mutex.lock();
    if(!m_Components.count(_component->getComponentName()))
    {
        m_Components[_component->getComponentName()] = std::shared_ptr<Component>(_component);
    }
    m_Mutex.unlock();
}
bool Entity::hasComponent(std::string _componentName)
{
    m_Mutex.lock();
    bool ret = m_Components.count(_componentName);
    m_Mutex.unlock();
    return ret;
}
std::shared_ptr<Component> Entity::removeComponent(std::string _componentName)
{
    m_Mutex.lock();
    std::shared_ptr<Component> ret;
    if(m_Components.count(_componentName))
    {
        ret = m_Components[_componentName];
        m_Components.erase(_componentName);
    }
    m_Mutex.unlock();
    return ret;
}
void Entity::deleteComponent(std::string _componentName)
{
    m_Mutex.lock();
    if(m_Components.count(_componentName))
    {
        m_Components[_componentName].reset();
        m_Components.erase(_componentName);
    }
    m_Mutex.unlock();
}

