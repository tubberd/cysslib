#include <cyss/util/InputMap.hpp>

#include <GLFW/glfw3.h>

using namespace cyss;

InputMap::InputMap(InputManager* mgr)
{
    m_Manager = mgr;
}

InputMap::~InputMap()
{
    
}

void InputMap::mapKeyboardButton(int _buttonID, KeyboardButton _deviceButton)
{
    m_KeyboardButtonMap[_deviceButton] = _buttonID;
}

void InputMap::mapMouseButton(int _buttonID, MouseButton _deviceButton)
{
    m_MouseButtonMap[_deviceButton] = _buttonID;
}

void InputMap::mapJoystickAxis(int _axisID, Joystick _joystick, JoystickAxis _deviceAxis)
{
    m_JoystickAxisMap[_joystick][_deviceAxis] = _axisID;
}

void InputMap::mapJoystickButton(int _buttonID, Joystick _joystick, JoystickButton _deviceButton)
{
    m_JoystickButtonMap[_joystick][_deviceButton] = _buttonID;
}

glm::vec2 InputMap::getMousePosition()
{
    return m_MousePosition;
}

bool InputMap::getBoolWasDown(int _buttonID)
{
    return m_ButtonDownMap[_buttonID];
}

float InputMap::getFloatAxis(int _axisID)
{
    return m_AxisMap[_axisID];
}

InputManager* InputMap::getInputManager()
{
    return m_Manager;
}