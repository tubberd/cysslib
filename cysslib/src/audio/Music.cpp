#include <cyss/audio/Music.hpp>

#include <SFML/Audio.hpp>

using namespace cyss;

Music::Music(std::string _filename)
{
    sf::Music* m = new sf::Music();
    m->openFromFile(_filename);
    m_MusicHandle = (void*)m;
}

Music::~Music()
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    delete m;
}

void Music::play()
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->play();
}

void Music::pause()
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->pause();
}

void Music::stop()
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->stop();
}

void Music::setPitch(float _pitch)
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->setPitch(_pitch);
}

void Music::setShouldLoop(bool _shouldLoop)
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->setLoop(_shouldLoop);
}

void Music::setVolume(float _volume)
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->setVolume(_volume);
}

void Music::setOffsetS(float _offsetS)
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->setPlayingOffset(sf::seconds(_offsetS));
}

void Music::setOffsetMS(int _offsetMS)
{
    sf::Music* m = (sf::Music*)m_MusicHandle;
    m->setPlayingOffset(sf::milliseconds(_offsetMS));
}

