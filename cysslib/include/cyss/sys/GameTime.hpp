#ifndef CYSSLIB_SYS_GAMETIME_HPP
#define CYSSLIB_SYS_GAMETIME_HPP

#include <chrono>

namespace cyss
{
    class GameTime
    {
    public:
        GameTime();
        ~GameTime();
        
        void update();
        
        int getElapsedMS();
        int getElapsedNS();
        int getElapsedS();
    private:
        std::chrono::high_resolution_clock::time_point m_Last;
    };
}

#endif