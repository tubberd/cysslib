#ifndef CYSSLIB_SYS_TASK_HPP
#define CYSSLIB_SYS_TASK_HPP

#include <functional>
#include <mutex>

namespace cyss
{
    class Task
    {
    public:
        Task();
        Task(std::function<void()> _function);
        ~Task();
        
        void _execute();
        bool getFinished();
        void setFinished();
    protected:
        std::function<void()> m_Function;
        std::mutex m_Mutex;
        bool m_IsFinished;
        virtual void execute();
        bool m_HasFunction;
    };
    
    class LoopTask : public Task
    {
    public:
        LoopTask();
        LoopTask(std::function<void()> _function);
        ~LoopTask();
        
        void setMaxFrequency(float _frequency);
        
        float getMaxFrequency();
    private:
        float m_MaxFrequency;
    };
}

#endif