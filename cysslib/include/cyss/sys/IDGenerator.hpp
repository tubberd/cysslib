#ifndef CYSSLIB_SYS_IDGENERATOR_HPP
#define CYSSLIB_SYS_IDGENERATOR_HPP

#include <cyss/sys/Singleton.hpp>
#include <cstddef>
#include <string>
#include <functional>

namespace cyss
{
    class IDGenerator : public Singleton<IDGenerator>
    {
    public:
        size_t getNextID();
        size_t getID(std::string _param);
    private:
        friend class Singleton<IDGenerator>;
        IDGenerator();
        ~IDGenerator();
        
        size_t m_IDSequence;
        std::hash<std::string> m_StringHasher;
    };
}

#endif