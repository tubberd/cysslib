#ifndef CYSSLIB_SYS_GAME_HPP
#define CYSSLIB_SYS_GAME_HPP

namespace cyss
{
    class Game
    {
    public:
        Game();
        virtual ~Game();
        void run();
        void shutdown();
    private:
        bool m_IsRunning;
    protected:
        virtual void initialize() = 0;
        virtual void loadContent() = 0;
        virtual void unloadContent() = 0;
        virtual void update() = 0;
        virtual void draw() = 0;
    };
}

#endif