#ifndef CYSSLIB_SYS_KERNEL_HPP
#define CYSSLIB_SYS_KERNEL_HPP

#include <mutex>
#include <thread>
#include <vector>
#include <queue>
#include <unordered_map>
#include <memory>
#include <condition_variable>

#include <cyss/sys/Singleton.hpp>
#include <cyss/sys/Task.hpp>

namespace cyss
{
    enum KernelState
    {
        Running,
        WorkAvailable,
        Shutdown
    };
    class Kernel : public Singleton<Kernel>
    {
    public:
        void boot(unsigned int _workerCount);
        void run();
        void shutdown();
        
        void addTask(std::shared_ptr<Task> _task);
        void addLoopTask(std::shared_ptr<LoopTask> _task);
    private:
        friend class Singleton<Kernel>;
        Kernel();
        ~Kernel();
        
        bool m_IsRunning;
        unsigned int m_WorkerCount;
        KernelState m_KernelState;
        
        std::vector<std::thread> m_Workers;
        std::vector<std::thread> m_LoopWorkers;
        
        std::queue<std::shared_ptr<Task>> m_Tasks;
        std::queue<std::shared_ptr<LoopTask>> m_LoopTasks;
        
        std::mutex m_IsRunningMutex;
        std::mutex m_KernelStateMutex;
        std::mutex m_WorkersMutex;
        std::mutex m_LoopWorkersMutex;
        std::mutex m_TasksMutex;
        std::mutex m_LoopTasksMutex;
        
        std::condition_variable m_IsRunningCond;
        std::condition_variable m_KernelStateCond;
        
        void workTask(unsigned int _workerID);
    };
}

#endif