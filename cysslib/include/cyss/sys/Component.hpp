#ifndef CYSSLIB_SYS_COMPONENT_HPP
#define CYSSLIB_SYS_COMPONENT_HPP

#include <string>
#include <cstddef>
#include <mutex>

namespace cyss
{
    namespace ecs
    {
        class Component
        {
        public:
            Component(std::string _componentName);
            virtual ~Component();
            
            virtual Component* copyThis() = 0;
            
            std::string getComponentName();
            
            std::mutex Mutex;
        protected:
            std::string m_ComponentName;
            size_t m_UniqueID;
        };
    }
}

#endif