#ifndef CYSSLIB_UTIL_LOGGER_HPP
#define CYSSLIB_UTIL_LOGGER_HPP

#include <string>
#include <mutex>

#include <cyss/sys/Singleton.hpp>

namespace cyss
{
    class Logger : public Singleton<Logger>
    {
    public:
        void consoleLog(std::string _from, std::string _message);
    private:
        friend class Singleton<Logger>;
        Logger();
        ~Logger();
        
        std::mutex m_Mutex;
    };
}

#endif
