#ifndef CYSSLIB_PHYSICS_IHASBOUNDINGBOX_HPP
#define CYSSLIB_PHYSICS_IHASBOUNDINGBOX_HPP

#include <cyss/physics/BoundingBox.hpp>

namespace cyss
{
    class IHasBoundingBox
    {
    public:
        virtual BoundingBox& getBoundingBox() = 0;
        virtual void setBoundingBox(BoundingBox& _aabb) = 0;
    };
}

#endif