#ifndef CYSSLIB_PHYSICS_BOUNDINGBOX_HPP
#define CYSSLIB_PHYSICS_BOUNDINGBOX_HPP

#include <cyss/physics/vec4f.hpp>

namespace cyss
{
    class BoundingBox
    {
    public:
        BoundingBox();
        BoundingBox(vec4f& _center, vec4f& _halfwidth);
        ~BoundingBox();
        
        bool intersects(BoundingBox& _other);
    private:
        vec4f m_Center;
        vec4f m_Halfwidth;
    };
}

#endif 