#ifndef CYSSLIB_GRAPHICS_2D_FONT_HPP
#define CYSSLIB_GRAPHICS_2D_FONT_HPP

#include <string>

namespace cyss
{
    class MonochromeFont
    {
    public:
        MonochromeFont(std::string _filename, float _size);
        ~MonochromeFont();
        
        void setSize(float _size);
        void setForegroundColor(unsigned char _r, unsigned char _g, unsigned char _b);
        void draw(float _x, float _y, std::string _text);
    private:
        void* m_Handle;
    };
}

#endif