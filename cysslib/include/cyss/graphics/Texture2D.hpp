#ifndef CYSSLIB_GRAPHICS_TEXTURE2D_HPP
#define CYSSLIB_GRAPHICS_TEXTURE2D_HPP

#include <string>

namespace cyss
{
    class Texture2D
    {
    public:
        Texture2D(unsigned int _width, unsigned int _height, bool hasAlpha);
        ~Texture2D();
        
        void setActive(unsigned int _activeIndex);
        void loadFile(std::string _filename);
        
        void setAt(unsigned int _x, unsigned int _y, unsigned char R, unsigned char G, unsigned char B);
        void setAt(unsigned int _x, unsigned int _y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
        
        unsigned int getWidth();
        unsigned int getHeight();
        
        void debug();
    private:
        unsigned int m_ID;
        unsigned int m_ActiveIndex;
        unsigned int m_Width, m_Height;
    };
}

#endif